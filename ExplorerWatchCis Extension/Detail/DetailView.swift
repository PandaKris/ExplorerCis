//
//  DetailView.swift
//  ExplorerWatchCis Extension
//
//  Created by Kristanto Sean on 13/07/21.
//

import SwiftUI

struct DetailView: View {
    
    @ObservedObject var explorer: ExplorerWrapper
    
    var body: some View {
        ScrollView {
            VStack(alignment: .center) {
                if (explorer.fields.photo != nil) {
                    RemoteImage(url: explorer.fields.photo?[0].thumbnails?.large?.url ?? "")
                        .aspectRatio(contentMode: .fit)
                }
                Text(explorer.fields.name)
                Text(explorer.fields.expertise)
                Text(explorer.fields.shift)

            }
        }
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(explorer: ExplorerWrapper(id: "test", fields: Explorer(name: "Name", photo: [ExplorerPhoto(url:"https://image.shutterstock.com/image-photo/mountains-under-mist-morning-amazing-260nw-1725825019.jpg", fileName: "Test", thumbnail: nil)], expertise: "Expertise", team: ["Team"], shift: "Shift", lqMentor: "Cis"), createdAt: "String", isFavorited: false, tags: ["Gamer"]))
    }
}
