//
//  ContentView.swift
//  ExplorerWatchCis Extension
//
//  Created by Kristanto Sean on 13/07/21.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var viewModel = ContentViewModel()
    
    var body: some View {
        List {
            ForEach(viewModel.explorers.list, id: \.id) { explorer in
                NavigationLink(destination: DetailView(explorer: explorer)) {
                    VStack(alignment: .leading) {
                        Text(explorer.fields.name)
                        Text(explorer.fields.expertise)
                        Text(explorer.fields.shift)
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
