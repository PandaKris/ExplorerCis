//
//  ExplorerCisApp.swift
//  ExplorerWatchCis Extension
//
//  Created by Kristanto Sean on 13/07/21.
//

import SwiftUI

@main
struct ExplorerCisApp: App {
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
