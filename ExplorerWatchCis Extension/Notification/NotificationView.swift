//
//  NotificationView.swift
//  ExplorerWatchCis Extension
//
//  Created by Kristanto Sean on 13/07/21.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
