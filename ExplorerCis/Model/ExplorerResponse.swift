//
//  Explorer.swift
//  ExplorerCis
//
//  Created by Kristanto Sean on 22/04/21.
//

import Foundation

public struct ExplorerResponse: Decodable {
    
    var records: [ExplorerWrapper]
    var offset: String?
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        records = try values.decode([ExplorerWrapper].self, forKey: .records)
        offset = try values.decodeIfPresent(String.self, forKey: .offset)
    }

    private enum CodingKeys: String, CodingKey {
        case records = "records"
        case offset = "offset"
    }
}


public class ExplorerWrapper: Decodable, ObservableObject {
    var id: String
    var fields: Explorer
    var createdTime: String
    
    // Custom Variabless
    @Published var isFavorited: Bool = false
    @Published var tags: [String]?
    
    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        fields = try values.decode(Explorer.self, forKey: .fields)
        createdTime = try values.decode(String.self, forKey: .createdTime)
    }
    
    public init(id: String, fields: Explorer, createdAt: String, isFavorited: Bool, tags: [String]?) {
        self.id = id
        self.fields = fields
        self.createdTime = createdAt
        
        self.isFavorited = isFavorited
        self.tags = tags
    }

    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case fields = "fields"
        case createdTime = "createdTime"
    }
        
}



public struct Explorer: Decodable {
    var name: String
    var photo: [ExplorerPhoto]?
    var expertise: String
    var team: [String]?
    var shift: String
    var lqMentor: String?
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decode(String.self, forKey: .name)
        photo = try values.decodeIfPresent([ExplorerPhoto].self, forKey: .photo)
        expertise = try values.decode(String.self, forKey: .expertise)
        team = try values.decodeIfPresent([String].self, forKey: .team)
        shift = try values.decode(String.self, forKey: .shift)
        lqMentor = try values.decodeIfPresent(String.self, forKey: .lqMentor)
    }
    
    public init (name: String, photo: [ExplorerPhoto], expertise: String, team: [String], shift: String, lqMentor: String?){
        self.name = name
        self.photo = photo
        self.expertise = expertise
        self.team = team
        self.shift = shift
        self.lqMentor = lqMentor
    }

    private enum CodingKeys: String, CodingKey {
        case name = "Name"
        case photo = "Photo"
        case expertise = "Expertise"
        case team = "Team"
        case shift = "Shift"
        case lqMentor = "LQ Mentor"
    }
}

public struct ExplorerPhoto: Decodable {
    var url: String
    var fileName: String
    var thumbnails: ExplorerThumbnail?
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        url = try values.decode(String.self, forKey: .url)
        fileName = try values.decode(String.self, forKey: .fileName)
        thumbnails = try values.decodeIfPresent(ExplorerThumbnail.self, forKey: .thumbnails)
    }
    
    public init (url: String, fileName: String, thumbnail: ExplorerThumbnail?) {
        self.url = url
        self.fileName = fileName
        self.thumbnails = thumbnail
    }

    private enum CodingKeys: String, CodingKey {
        case url = "url"
        case fileName = "filename"
        case thumbnails = "thumbnails"
    }
}


public struct ExplorerThumbnail: Decodable {
    var small: Thumbnail?
    var large: Thumbnail?
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        small = try values.decodeIfPresent(Thumbnail.self, forKey: .thumbnailSmall)
        large = try values.decodeIfPresent(Thumbnail.self, forKey: .thumbnailLarge)
    }
    
    private enum CodingKeys: String, CodingKey {
        case thumbnailSmall = "small"
        case thumbnailLarge = "large"
    }
}


public struct Thumbnail: Decodable {
    var url: String
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        url = try values.decode(String.self, forKey: .url)
    }
    
    private enum CodingKeys: String, CodingKey {
        case url = "url"
    }
}
