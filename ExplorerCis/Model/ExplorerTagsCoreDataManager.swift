//
//  ExplorerManager.swift
//  ExplorerCis
//
//  Created by Kristanto Sean on 22/04/21.

import Foundation
import CoreData

public class ExplorerTagsCoreDataManager {
        
    var context: NSManagedObjectContext
    
    init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    func getAllTags() -> [String] {
        var list: [String] = []
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "TagsCoreData")
        do {
            let results = try context.fetch(request)
            for result in results {
                let tagString = (result as! NSManagedObject).value(forKey: "tag_name") as! String
                list.append(tagString)
            }
        } catch {
            print(error)
        }
        return list
    }
    
    func getUserTags(id: String) -> [String] {
        do {
            let requestTagsRel = NSFetchRequest<NSFetchRequestResult>(entityName: "ExplorerTagsCoreData")
            let requestTags = NSFetchRequest<NSFetchRequestResult>(entityName: "TagsCoreData")

            requestTagsRel.predicate = NSPredicate(format: "explorer_id = %@", id)
            let results = try context.fetch(requestTagsRel)
            var tags: [String] = []
            for result in results {
                if let tagID = (result as! NSManagedObject).value(forKey: "tag_id") as? String {
                    requestTags.predicate = NSPredicate(format: "tag_id = %@", tagID)
                    let tagResult = try context.fetch(requestTags)
                    if tagResult.count > 0 {
                        let tagString = (tagResult[0] as! NSManagedObject).value(forKey: "tag_name") as! String
                        tags.append(tagString)
                    }
                }
            }
            return tags
        } catch {
            print(error)
        }
        
        return []
    }
    
    func saveTag(id: String, text: String) -> Bool{
        do {
            let requestTags = NSFetchRequest<NSFetchRequestResult>(entityName: "TagsCoreData")
            requestTags.predicate = NSPredicate(format: "tag_name = %@", text)
            
            var tagID = ""
            let results = try context.fetch(requestTags)
            if results.count > 0 {
                let data = results[0] as! NSManagedObject
                tagID = (data.value(forKey: "tag_id") as! UUID).uuidString
            }
            
            if tagID.isEmpty {
                let entity = NSEntityDescription.entity(forEntityName: "TagsCoreData", in: context)
                let tag = NSManagedObject(entity: entity!, insertInto: context)
                tag.setValue(text, forKeyPath: "tag_name")
                tag.setValue(UUID(), forKey: "tag_id")
                try context.save()
                    
                let requestTags = NSFetchRequest<NSFetchRequestResult>(entityName: "TagsCoreData")
                requestTags.predicate = NSPredicate(format: "tag_name = %@", text)
                    
                let results = try context.fetch(requestTags)
                if results.count > 0 {
                    let data = results[0] as! NSManagedObject
                    tagID = (data.value(forKey: "tag_id") as! UUID).uuidString
                }
            }
            
            if tagID.isEmpty { return false }
            
            let requestTagsRelationship = NSFetchRequest<NSFetchRequestResult>(entityName: "ExplorerTagsCoreData")
            requestTagsRelationship.predicate = NSPredicate(format: "tag_id = %@ AND explorer_id = %@", tagID, id)
            let relResults = try context.fetch(requestTagsRelationship)
            if relResults.count > 0 { return false }
                
            let entity = NSEntityDescription.entity(forEntityName: "ExplorerTagsCoreData", in: context)
            let tag = NSManagedObject(entity: entity!, insertInto: context)
            tag.setValue(id, forKey: "explorer_id")
            tag.setValue(tagID, forKeyPath: "tag_id")
            try context.save()
          
            return true
        } catch {
            print(error)
        }
        return false
    }
    
    
    func deleteTags(id: String, tag: String) -> Bool {
        
        let requestTags = NSFetchRequest<NSFetchRequestResult>(entityName: "TagsCoreData")
        requestTags.predicate = NSPredicate(format: "tag_name = %@", tag.lowercased())
        
        do{
            let results = try context.fetch(requestTags)
            if results.count == 0 { return false }
            
            let data = results[0] as! NSManagedObject
            let tagID = (data.value(forKey: "tag_id") as! UUID).uuidString

            //TODO: if found, get ID and find relationship of explorer and tag id, else return false
            let requestTagsRelationship = NSFetchRequest<NSFetchRequestResult>(entityName: "ExplorerTagsCoreData")
            requestTagsRelationship.predicate = NSPredicate(format: "tag_id = %@ AND explorer_id = %@", tagID, id)

            let relResult = try context.fetch(requestTagsRelationship)
            if relResult.count == 0 { return false }

            //TODO: if found, delete that data, else return false
            context.delete(relResult[0] as! NSManagedObject)
            try context.save()

            //TODO: check relationship if there is other user using that tag_id, if no one using it, delete the tag
            let checkTagsRelationship = NSFetchRequest<NSFetchRequestResult>(entityName: "ExplorerTagsCoreData")
            checkTagsRelationship.predicate = NSPredicate(format: "tag_id = %@", tagID)
            
            let checkResult = try context.fetch(checkTagsRelationship)
            if checkResult.count == 0 {
                context.delete(data)
                try context.save()
            }
            
            return true
        } catch let error{
            print(error)
        }
        return false
    }
}
