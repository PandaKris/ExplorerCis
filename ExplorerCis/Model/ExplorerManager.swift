//
//  ExplorerManager.swift
//  ExplorerCis
//
//  Created by Kristanto Sean on 22/04/21.

import Foundation
import Alamofire

public class ExplorerManager {
    
    var explorers: ExplorerResponse?
    let urlString = "https://api.airtable.com/v0/appl5upXW9B14V2S2/Explorer?api_key=key5iq7nmQ10l8N9X"
    
    init() {
        explorers = nil
    }
    
    func getData(offset: String) -> DataRequest {
        var url = urlString
        if !offset.isEmpty {
            url = url + "&offset=" + offset
        }
        let request = AF.request(url)
        return request
    }
}
