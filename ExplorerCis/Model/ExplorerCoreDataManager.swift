//
//  ExplorerManager.swift
//  ExplorerCis
//
//  Created by Kristanto Sean on 22/04/21.

import Foundation
import CoreData

public class ExplorerCoreDataManager {
        
    var context: NSManagedObjectContext
    
    init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    func getFavoriteByID(id: String) -> Bool {
        do {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ExplorerCoreData")
            request.predicate = NSPredicate(format: "id = %@", id)

            let results = try context.fetch(request)
            if results.count > 0 {
                return ((results[0] as! NSManagedObject).value(forKey: "isFavorite") as? Bool)!
            }
        } catch {
            print(error)
        }
        
        return false        
    }
    
    
    
    func saveFavorite(id: String, isFavorite: Bool) -> Bool {
        do {
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ExplorerCoreData")
            request.predicate = NSPredicate(format: "id = %@", id)

            let results = try context.fetch(request)
            if results.count > 0 {
                let data = results[0] as! NSManagedObject
                data.setValue(isFavorite, forKeyPath: "isFavorite")
                try context.save()
                return true
            }
             
            let entity = NSEntityDescription.entity(forEntityName: "ExplorerCoreData", in: context)
            let explorer = NSManagedObject(entity: entity!, insertInto: context)
            explorer.setValue(id, forKeyPath: "id")
            explorer.setValue(isFavorite, forKeyPath: "isFavorite")
            try context.save()
            return true
        } catch {
            print(error)
        }
        
        return false

    }
}
