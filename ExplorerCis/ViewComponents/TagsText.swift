//
//  TagsText.swift
//  ExplorerCis
//
//  Created by Kristanto Sean on 30/04/21.
//

import SwiftUI

struct TagsText: View {
    
    var text: String
    var isSelected: Bool = false
    
    var body: some View {
        
        if (isSelected) {
            tags(text: text).background(RoundedRectangle(cornerRadius: 10).fill(Color.black))
        } else {
            tags(text: text).background(RoundedRectangle(cornerRadius: 10).fill(Color.blue))
        }
            
    }
}

func tags(text: String) -> some View {
    return Text(text)
        .font(.caption2)
        .foregroundColor(Color.white)
        .padding(EdgeInsets(top: 4, leading: 8, bottom: 4, trailing: 8))
}

struct TagsText_Previews: PreviewProvider {
    static var previews: some View {
        TagsText(text: "test")
    }
}
