//
//  ExplorerDetailView.swift
//  ExplorerCis
//
//  Created by Kristanto Sean on 23/04/21.
//

import SwiftUI
import CoreData

struct ExplorerDetailView: View {
    
    @State private var isPresented: Bool = false
    @State private var text: String = ""
        
    @ObservedObject var explorer: ExplorerWrapper
    @ObservedObject var viewModel = ExplorerDetailViewModel()
    
    var body: some View {
        ScrollView {
            ZStack {
                VStack(alignment: .leading){
                    if (explorer.fields.photo != nil) {
                        RemoteImage(url: explorer.fields.photo?[0].thumbnails?.large?.url ?? "")
                            .aspectRatio(contentMode: .fit)
                        
                    }
                    HStack{
                        Text(explorer.fields.name).bold().padding(EdgeInsets(top: 10, leading: 10, bottom: 0, trailing: 0)).font(.title)
                        Spacer()
                        FavoriteButton(explorer: explorer, viewModel: viewModel)
                    }
                    
                    ScrollView(.horizontal, showsIndicators: false){
                        HStack{
                            if let tags = explorer.tags {
                                ForEach(tags, id:\.self) { tag in
                                    Menu {
                                        Button("Delete") {
                                            if (viewModel.deleteTag(explorer: explorer, tag: tag)) {
                                                explorer.tags?.remove(at: explorer.tags!.firstIndex(of: tag)!)
                                            }
                                        }
                                    } label: {
                                        TagsText(text: tag)
                                        
                                    }
                                }
                            }
                        }
                    }.padding(EdgeInsets(top: 3, leading: 8, bottom: 3, trailing: 8))
                    
                    HStack {
                        Text("ID")
                        Spacer()
                        Text(explorer.id)
                    }.padding().background(Color.yellow)
                    
                    HStack {
                        Text("Expertise")
                        Spacer()
                        Text(explorer.fields.expertise)
                    }.padding()
                    
                    HStack {
                        Text("Shift")
                        Spacer()
                        Text(explorer.fields.shift)
                    }.padding().background(Color.yellow)
                    
                    HStack {
                        Text("LQ Mentor")
                        Spacer()
                        Text(explorer.fields.lqMentor ?? "")
                    }.padding()
                    
                }
                
            }
            
        }.navigationBarTitle(explorer.fields.name, displayMode: .inline)
        .toolbar{
            ToolbarItemGroup(placement: .navigationBarTrailing) {
                Button(action: {
                    showTagsAlert()
                }, label: {
                    Image(systemName: "tag")
                })
            }
        }
    }
    
    
    
    func showTagsAlert() {
        let alert = UIAlertController(title: "Add Tags", message: "Add your tags here", preferredStyle: .alert)
        
        alert.addTextField { (tags) in
            tags.placeholder = "Tag"
        }
        
        let save = UIAlertAction(title: "Save", style: .default, handler: { (_) in
            let text = alert.textFields![0].text!
            if viewModel.saveTags(explorer: explorer, text: text) {
                explorer.tags?.append(text.lowercased())
            }
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        alert.addAction(cancel)
        alert.addAction(save)
        
        UIApplication.shared.windows.first?.rootViewController?.present(alert, animated: true, completion: {
            
        })
        
    }
    
}

struct FavoriteButton: View {
    
    var explorer: ExplorerWrapper
    var viewModel: ExplorerDetailViewModel
    
    var body: some View {
        Button(action: {
            viewModel.setFavorite(data: explorer, isFavorite: !explorer.isFavorited)
            explorer.isFavorited = viewModel.isFavorited
        }) {
            Image(systemName: explorer.isFavorited ? "heart.fill" : "heart").padding()
        }
    }
    
}

struct ExplorerDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ExplorerDetailView(explorer: ExplorerWrapper(id: "test", fields: Explorer(name: "Name", photo: [ExplorerPhoto(url:"https://image.shutterstock.com/image-photo/mountains-under-mist-morning-amazing-260nw-1725825019.jpg", fileName: "Test", thumbnail: nil)], expertise: "Expertise", team: ["Team"], shift: "Shift", lqMentor: "Cis"), createdAt: "String", isFavorited: false, tags: ["Gamer"]))
    }
}
