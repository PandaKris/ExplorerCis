//
//  ExplorerDetailViewModel.swift
//  ExplorerCis
//
//  Created by Kristanto Sean on 23/04/21.
//

import Foundation
import CoreData
import SwiftUI


final class ExplorerDetailViewModel: ObservableObject {
    
    @Published var explorerState: State = State.idle
    @Published var isFavorited: Bool = false
    
    func setFavorite(data: ExplorerWrapper, isFavorite: Bool) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        if (ExplorerCoreDataManager(context: context).saveFavorite(id: data.id, isFavorite: isFavorite)) {
            self.isFavorited = isFavorite
        }
    }
    
    
    func saveTags(explorer: ExplorerWrapper, text: String) -> Bool {
        if text.isEmpty { return false }
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        return ExplorerTagsCoreDataManager(context: context).saveTag(id: explorer.id, text: text.lowercased())
    }
    
    
    func deleteTag(explorer: ExplorerWrapper, tag: String) -> Bool {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        return ExplorerTagsCoreDataManager(context: context).deleteTags(id: explorer.id, tag: tag.lowercased())
    }
    
    
    enum State {
        case idle
        case loading
        case success(Bool)
        case error(String)
    }

    
}
