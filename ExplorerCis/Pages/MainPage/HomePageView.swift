//
//  HomePageView.swift
//  ExplorerCis
//
//  Created by Kristanto Sean on 04/05/21.
//

import SwiftUI

struct HomePageView: View {
    
    var body: some View {
        TabView {
            ExplorerListView()
                .tabItem {
                    Label("List", systemImage: "list.dash")
                }

            FilterPageView()
                .tabItem {
                    Label("Filter", systemImage: "line.horizontal.3.decrease.circle")
                }
            
            SettingsPageView()
                .tabItem {
                    Label("Settings", systemImage: "gearshape")
                }

        }
    }
}

struct HomePageView_Previews: PreviewProvider {
    static var previews: some View {
        HomePageView()
    }
}
