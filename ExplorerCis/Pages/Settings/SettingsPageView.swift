//
//  SettingsPageView.swift
//  ExplorerCis
//
//  Created by Kristanto Sean on 04/05/21.
//

import SwiftUI

struct SettingsPageView: View {
    var body: some View {
        Text("Settings")
    }
}

struct SettingsPageView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsPageView()
    }
}
