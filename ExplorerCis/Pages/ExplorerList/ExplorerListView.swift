//
//  ContentView.swift
//  ExplorerCis
//
//  Created by Kristanto Sean on 22/04/21.
//

import SwiftUI
import UIKit

struct ExplorerListView: View {
    
    @StateObject var viewModel = ExplorerListViewModel()
    
    @State var searchText : String = ""
    
    
    var body: some View {
        NavigationView {
            stateView(state: viewModel.explorerState)
            .navigationBarTitle(Text("Explorers"))
            .toolbar {
                ToolbarItemGroup(placement: .navigationBarTrailing) {
                    Button(action: {
                        viewModel.toggleFavorites()
                    }, label: {
                        Image(systemName: viewModel.favoritesFilter ? "heart.fill" : "heart")
                            .renderingMode(.original)
                    })
                    Button(action: {
                        viewModel.loadData(offset:"", reload:true)
                        viewModel.loadTags()
                    }) {
                        Image(systemName: "arrow.clockwise").renderingMode(.original)
                    }

                }
            }
        }
    }
    
    @ViewBuilder
    func stateView(state: ExplorerListViewModel.State) -> some View {
            switch state {
            case .idle:
                ProgressView()
            case .loading:
                ProgressView()
            case .success:
                 
                    GeometryReader { g in
                        ScrollView {
                            VStack {
                                SearchBar(text: $searchText)
                                tagView(viewModel: viewModel)
                                List {
                                    ForEach(viewModel.explorers.list, id: \.id) { explorer in
                                        if (viewModel.itemDisplayed(search: searchText, explorer: explorer)) {
                                            ZStack {
                                                NavigationLink(
                                                    destination: ExplorerDetailView(explorer: explorer),
                                                    label: {
                                                        EmptyView()
                                                    })
                                                    .onReceive(explorer.$isFavorited) { _ in
                                                        self.viewModel.objectWillChange.send()
                                                        viewModel.loadTags()
                                                    }
                                                    .onReceive(explorer.$tags) { _ in
                                                        self.viewModel.objectWillChange.send()
                                                        viewModel.loadTags()
                                                    }
                                                    .opacity(0.0)
                                                    .buttonStyle(PlainButtonStyle())
                                                explorerCells(explorer: explorer)
                                            }
                                        }
                                    }
                                }.listRowInsets(.init()).frame(width: g.size.width, height: g.size.height, alignment: .center)
                            }
                        }
                    }
            case .error(let error):
                Text(error?.localizedDescription ?? "Unknown Error!")
            }
            
    }
    
    func explorerCells(explorer: ExplorerWrapper) -> some View {
        return HStack{
            if (explorer.fields.photo != nil) {
                RemoteImage(url: explorer.fields.photo?[0].thumbnails?.large?.url ?? "")
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 80, height: 80)
            }
            VStack(alignment:.leading){
                HStack{
                    Text(explorer.fields.name).bold().lineLimit(1)
                    Spacer()
                    Image(systemName: explorer.isFavorited ? "heart.fill" : "heart")
                }
                Text(explorer.fields.expertise).lineLimit(1)
                ScrollView(.horizontal, showsIndicators: false){
                    HStack{
                        if let tags = explorer.tags {
                            ForEach(tags, id:\.self) { tag in
                                TagsText(text: tag)
                            }
                        }
                    }
                }
                Spacer()
            }.padding()
            Spacer()
        }.listRowInsets(EdgeInsets())
    }
    
    
    func tagView(viewModel: ExplorerListViewModel) -> some View {
        return ScrollView(.horizontal, showsIndicators: false) {
            HStack{
                if let tags = viewModel.tags.list {
                    ForEach(tags, id:\.self) { tag in
                        Button(action: {
                            viewModel.selectFilter(tag: tag)
                        }, label: {
                            TagsText(text: tag, isSelected: viewModel.filterBy == tag)
                        })
                    }
                }
            }.padding(EdgeInsets(top: 3, leading: 8, bottom: 3, trailing: 8))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ExplorerListView()
                .previewDevice("iPhone 11")
        }
    }
}
