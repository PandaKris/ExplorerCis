
//
//  ExplorerListVIewModel.swift
//  ExplorerCis
//
//  Created by Kristanto Sean on 22/04/21.
//

import Foundation
import Alamofire
import CoreData

import UIKit

final class ExplorerListViewModel: ObservableObject {
    
    @Published var explorerState: State = State.idle
    @Published var explorers = ExplorerList(list: [])
    @Published var tags = TagsList(list: [])
    @Published var filterBy: String = ""
    @Published var favoritesFilter: Bool = false
    
    init() {
        loadData(offset: "", reload: true)
        loadTags()
    }
    
    
    func loadData(offset: String, reload: Bool) {
        self.explorerState = .loading
        let explorerManager = ExplorerManager()
        explorerManager.getData(offset: offset).responseDecodable(of: ExplorerResponse.self) { (data) in
            if (data.error != nil) {
                print(data.error as Any)
                self.explorerState = .error(data.error)
            } else {
                let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                
                // populate with coredata
                if let records = data.value?.records {
                    
                    // check favorites
                    for n in 0...records.count-1 {
                        records[n].isFavorited = ExplorerCoreDataManager(context: context).getFavoriteByID(id: records[n].id)
                    }
                
                    // check tags
                    for n in 0...records.count-1 {
                        records[n].tags = ExplorerTagsCoreDataManager(context: context).getUserTags(id: records[n].id)
                    }
                                            
                    self.explorerState = .success
                    if reload {
                        self.explorers = ExplorerList(list: records)
                    } else {
                        self.explorers = ExplorerList(list: self.explorers.list + records)
                    }
                } else {
                    self.explorerState = .success
                    if reload {
                        self.explorers = ExplorerList(list: data.value?.records ?? [])
                    }
                }
                if data.value?.offset != nil {
                    self.loadData(offset: (data.value?.offset)!, reload: false)
                }
            }
        }
    }
    
    
    func loadTags() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        self.tags = TagsList(list: ExplorerTagsCoreDataManager(context: context).getAllTags())
    }
    
    func selectFilter(tag: String) {
        if (tag == self.filterBy) {
            self.filterBy = ""
        } else {
            self.filterBy = tag
        }
    }
    
    func toggleFavorites() {
        self.favoritesFilter = !self.favoritesFilter
    }
    
    
    func itemDisplayed(search: String, explorer: ExplorerWrapper) -> Bool {
        if (search.isEmpty || explorer.fields.name.lowercased().contains(search.lowercased())) {
            if (explorer.tags?.contains(self.filterBy) ?? true || self.filterBy.isEmpty) {
                if ((self.favoritesFilter && explorer.isFavorited) || !self.favoritesFilter) {
                    return true
                }
            }
        }
        
        return false
    }


    enum State {
        case idle
        case loading
        case success
        case error(AFError?)
    }
}



final class ExplorerList: ObservableObject {
    @Published var list: [ExplorerWrapper] = []
    
    init(list: [ExplorerWrapper]) {
        self.list = list
    }
}

final class TagsList: ObservableObject {
    @Published var list: [String] = []
    
    init(list: [String]) {
        self.list = list
    }
}
