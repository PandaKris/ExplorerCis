
//
//  ExplorerListVIewModel.swift
//  ExplorerCis
//
//  Created by Kristanto Sean on 22/04/21.
//

import Foundation
import Alamofire
import CoreData

final class FilterPageViewModel: ObservableObject {
    
    @Published var explorerState: State = State.idle
    @Published var explorers = ExplorerList(list: [])

    @Published var filterShift: String = "All"
    @Published var filterExpertise: String = "All"
    
    @Published var favoritesFilter: Bool = false
    
    @Published var shiftArray: [String] = []
    @Published var expertiseArray: [String] = []
    
    init() {
        loadData(offset: "", reload: true)
    }
    
    
    func loadData(offset: String, reload: Bool) {
        self.explorerState = .loading
        let explorerManager = ExplorerManager()
        explorerManager.getData(offset: offset).responseDecodable(of: ExplorerResponse.self) { (data) in
            if (data.error != nil) {
                print(data.error as Any)
                self.explorerState = .error(data.error)
            } else {
                let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ExplorerCoreData")
                let requestTagsRel = NSFetchRequest<NSFetchRequestResult>(entityName: "ExplorerTagsCoreData")
                let requestTags = NSFetchRequest<NSFetchRequestResult>(entityName: "TagsCoreData")
                
                // populate with coredata
                if let records = data.value?.records {
                    // check favorites

                    self.shiftArray = []
                    self.shiftArray.append("All")

                    self.expertiseArray = []
                    self.expertiseArray.append("All")

                    do {
                        for n in 0...records.count-1 {
                            
                            if !self.shiftArray.contains(records[n].fields.shift) {
                                self.shiftArray.append(records[n].fields.shift)
                            }

                            if !self.expertiseArray.contains(records[n].fields.expertise) {
                                self.expertiseArray.append(records[n].fields.expertise)
                            }

                            request.predicate = NSPredicate(format: "id = %@", records[n].id)
                            let results = try context.fetch(request)
                            if results.count > 0 {
                                records[n].isFavorited = ((results[0] as! NSManagedObject).value(forKey: "isFavorite") as? Bool)!
                            }
                        }
                    } catch {
                        print(error)
                    }
                    
                    // check tags
                    do {
                        for n in 0...records.count-1 {
                            requestTagsRel.predicate = NSPredicate(format: "explorer_id = %@", records[n].id)
                            let results = try context.fetch(requestTagsRel)
                            var tags: [String] = []
                            for result in results {
                                if let tagID = (result as! NSManagedObject).value(forKey: "tag_id") as? String {
                                    requestTags.predicate = NSPredicate(format: "tag_id = %@", tagID)
                                    let tagResult = try context.fetch(requestTags)
                                    if tagResult.count > 0 {
                                        let tagString = (tagResult[0] as! NSManagedObject).value(forKey: "tag_name") as! String
                                        tags.append(tagString)
                                    }
                                }
                            }
                            records[n].tags = tags
                        }
                    } catch {
                        print(error)
                    }
                                            
                    self.explorerState = .success
                    if reload {
                        self.explorers = ExplorerList(list: records)
                    } else {
                        self.explorers = ExplorerList(list: self.explorers.list + records)
                    }
                } else {
                    self.explorerState = .success
                    if reload {
                        self.explorers = ExplorerList(list: data.value?.records ?? [])
                    }
                }
                if data.value?.offset != nil {
                    self.loadData(offset: (data.value?.offset)!, reload: false)
                }
            }
        }
    }
    
    func toggleFavorites() {
        self.favoritesFilter = !self.favoritesFilter
    }
    
    
    func itemDisplayed(explorer: ExplorerWrapper) -> Bool {
        if (filterShift == "All" || explorer.fields.shift.lowercased().contains(filterShift.lowercased())) {
            if (filterExpertise == "All" || explorer.fields.expertise.lowercased().contains(filterExpertise.lowercased())) {
                if ((self.favoritesFilter && explorer.isFavorited) || !self.favoritesFilter) {
                    return true
                }
            }
        }
        
        return false
    }


    enum State {
        case idle
        case loading
        case success
        case error(AFError?)
    }
}
