//
//  FilterPageView.swift
//  ExplorerCis
//
//  Created by Kristanto Sean on 04/05/21.
//

import SwiftUI

struct FilterPageView: View {
    
    @ObservedObject var viewModel = FilterPageViewModel()
    private var columnGrids = [GridItem(.flexible()), GridItem(.flexible())]
    
    var body: some View {
        NavigationView {
            stateView(state: viewModel.explorerState)
            .navigationBarTitle(Text("Filters"))
            .toolbar {
                ToolbarItemGroup(placement: .navigationBarTrailing) {
                    
                    Button(action: {
                        viewModel.toggleFavorites()
                    }, label: {
                        if (viewModel.favoritesFilter) {
                            Image(systemName: "heart.fill").renderingMode(.original)
                        } else {
                            Image(systemName: "heart").renderingMode(.original)
                        }
                    })
                    
                    Button(action: {
                        viewModel.loadData(offset:"", reload:true)
                    }) {
                        Image(systemName: "arrow.clockwise").renderingMode(.original)
                    }

                }
            }
        }
    }
    
    @ViewBuilder
    func stateView(state: FilterPageViewModel.State) -> some View {
            switch state {
            case .idle:
                ProgressView()
            case .loading:
                ProgressView()
            case .success:
                ScrollView {
                    Picker(selection: $viewModel.filterExpertise, label: Text("")) {
                        ForEach(viewModel.expertiseArray, id: \.self) {
                            Text($0)
                        }
                    }.pickerStyle(SegmentedPickerStyle())
                    
                    Picker(selection: $viewModel.filterShift, label: Text("")) {
                        ForEach(viewModel.shiftArray, id: \.self) {
                            Text($0)
                        }
                    }.pickerStyle(SegmentedPickerStyle())
                    
                    LazyVGrid(columns: columnGrids, spacing:20) {
                        ForEach(viewModel.explorers.list, id: \.id) { explorer in
                            if (viewModel.itemDisplayed(explorer: explorer)) {
                                NavigationLink(
                                    destination: ExplorerDetailView(explorer: explorer),
                                    label: {
                                        explorerCollectionCell(explorer: explorer)
                                    })
                                    .onReceive(explorer.$isFavorited) { _ in
                                        self.viewModel.objectWillChange.send()
                                    }
                                    .onReceive(explorer.$tags) { _ in
                                        self.viewModel.objectWillChange.send()
                                    }
                                    .buttonStyle(PlainButtonStyle())
                            }
                        }
                    }
                }
            case .error(let error):
                Text(error?.localizedDescription ?? "Unknown Error!")
            }
            
    }
    
    func explorerCollectionCell(explorer: ExplorerWrapper) -> some View {
        return VStack{
            if (explorer.fields.photo != nil) {
                RemoteImage(url: explorer.fields.photo?[0].thumbnails?.large?.url ?? "")
                    .aspectRatio(contentMode: .fill)
                    .frame(minWidth:0, maxWidth:.infinity, minHeight: 60)
            }
            HStack {
                Text(explorer.fields.name).bold().lineLimit(1).font(.headline)
                Spacer()
            }
            HStack {
                Text(explorer.fields.expertise).lineLimit(1).font(.body)
                Spacer()
            }
            HStack {
                Text(explorer.fields.shift).lineLimit(1).font(.body)
                Spacer()
            }
            Spacer()
        }.padding(EdgeInsets(top: 3, leading: 3, bottom: 3, trailing: 3))
    }
}

struct FilterPageView_Previews: PreviewProvider {
    static var previews: some View {
        FilterPageView()
    }
}
